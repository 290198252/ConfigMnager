#ifndef CONFIG_H
#define CONFIG_H
#include "global.h"
#include "pattern/signleton.hpp"

class config {
public:
private:
    DECLARE_SINGLETON(config)
};
typedef Singletone<config> gConf;

#endif // CONFIG_H
