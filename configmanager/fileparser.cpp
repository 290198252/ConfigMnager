#include "fileparser.h"
#include <QDebug>
#include "yaml-cpp/parser.h"
#include "yaml-cpp/node/node.h"
#include "yaml-cpp/yaml.h"
#include <string>
#include <QJsonArray>

#include "global.h"

using namespace  std;

JsonParser::JsonParser()
{

}

int JsonParser::Parse(QString conf)
{
    Parser::Parse(conf);
    qDebug()<<conf;
    if(mFile->fileName().contains("json")){
        if(!mFile->open(QFileDevice::ReadWrite)){
            return -1;
        }
        QString dat = QString(mFile->readAll());
        _parseJson(dat);
    }else{

    }
    return 0;
}

int JsonParser::Parse(QStandardItemModel *myModel)
{
    auto tmpobj = QJsonObject();
    QModelIndex in = myModel->index(0, 0);
    if ( in.isValid() ) {
        for (int curIndex = 0; curIndex < myModel->rowCount(in); ++curIndex) {
            QModelIndex keyindex = myModel->index(curIndex, 0, in);
            QModelIndex dataindex = myModel->index(curIndex, 1, in);

            QStandardItem *keyItem = myModel->itemFromIndex(keyindex);
            QStandardItem *dataItem = myModel->itemFromIndex(dataindex);
            if(nullptr != keyItem) {
                if(!keyItem->hasChildren()){
                    this->mObj.insert(keyItem->data(Qt::DisplayRole).toString(),
                                      _QVariantToQjson(dataItem->data(Qt::EditRole)));
                }else{
                    QVariant dat = keyItem->data(Qt::UserRole);

                    qDebug()<<dat<<dat.type();
                    if(dat.type() == QVariant::List){
                        QJsonArray *arr = new QJsonArray;
                        this->_parseSubQVariant(keyItem,arr);
                        this->mObj.insert(keyItem->data(Qt::DisplayRole).toString(),
                                          QJsonValue(*arr));
                    }
                    if(dat.type() == QVariant::Map){
                        QJsonObject *obj = new QJsonObject;
                        qDebug()<<"shit"<<keyItem->data(Qt::DisplayRole)<<keyItem->data(Qt::EditRole);
                        this->_parseSubQVariant(keyItem,obj);
                        this->mObj.insert(keyItem->data(Qt::DisplayRole).toString(),
                                          QJsonValue(*obj));
                    }
                }
            }
        }
    }
    this->mDoc.setObject(this->mObj);
    qDebug()<<"data"<<mDoc.toJson().data();
}

void JsonParser::RenderQModelObject(QStandardItem* model)
{
    if(nullptr == model)
        return;
    qDebug()<<mObj;
    renderSubJsonQModel(mObj,model);
}



QString JsonParser::ToString()
{
    return QString(this->mDoc.toJson().data());
}

int JsonParser::_getJsonObjectFromString(QString jsonString)
{
    this->mDoc = QJsonDocument::fromJson(jsonString.toUtf8().data());
    if( mDoc.isNull() ){
        qDebug()<< "===> please check the string "<< jsonString.toLocal8Bit().data();
    }
    mObj = mDoc.object();
    qDebug()<<mDoc;
    return 0;
}

int JsonParser::renderSubJsonQModel(QJsonObject obj, QStandardItem *item)
{
    if(nullptr == item)
        return -1;
    for(auto itr = obj.begin();itr != obj.end();itr++){
            QJsonArray arr;
            switch (itr->type()) {
                case QJsonValue::Type::Array:
                {
                    arr =  itr->toArray();
                    QStandardItem* itemProject = new QStandardItem(gIconManager::Instance()->GetIcon("array"),itr.key());                    itemProject->setEditable(false);
                    itemProject->setEditable(false);
                    itemProject->setData(QVariant::List,Qt::UserRole);
                    renderSubJsonQArray(arr,itemProject);
                    item->appendRow(itemProject);
                    break;
                }
                case QJsonValue::Type::Object:
                {
                    QStandardItem* itemProject = new QStandardItem(gIconManager::Instance()->GetIcon("object"),itr.key());
                    itemProject->setData(QVariant::Map,Qt::UserRole);
                    itemProject->setEditable(false);
                    renderSubJsonQModel(itr->toObject(),itemProject);
                    item->appendRow(itemProject);
                    break;
                }
                case QJsonValue::Type::Bool:
                {
                    QStandardItem* itemProject = new QStandardItem(gIconManager::Instance()->GetIcon("tree_value_string"),itr.key());
                    QStandardItem* itemvalue = new QStandardItem(gIconManager::Instance()->GetIcon("bolean"),
                                                                 QString("%1").arg(itr.value().toBool()));
                    itemProject->setEditable(false);
                    itemvalue->setData(itr.value().toBool(),Qt::EditRole);
                    itemvalue->setData(QVariant::Bool,Qt::UserRole);

                    QList<QStandardItem*> list;
                    list<<itemProject<<itemvalue;
                    item->appendRow(list);
                    break;
                }
                case QJsonValue::Type::String:
                {
                    QStandardItem* itemkey = new QStandardItem(gIconManager::Instance()->GetIcon("tree_value_string"),itr.key());
                    QStandardItem* itemvalue = new QStandardItem(gIconManager::Instance()->GetIcon("tree_value_string"),itr->toString());
                    itemvalue->setData(QVariant(itr->toString()),Qt::EditRole);
                    itemvalue->setData(QVariant::String,Qt::UserRole);
                    itemkey->setEditable(false);
                    QList<QStandardItem*> list;
                    list<<itemkey<<itemvalue;
                    item->appendRow(list);

                    break;
                }
                case QJsonValue::Type::Null:
                {
                    break;
                }
                case QJsonValue::Type::Double:
                {
                    QStandardItem* itemProject = new QStandardItem(gIconManager::Instance()->GetIcon("tree_value_string"),itr.key());
                    QStandardItem* itemvalue = new QStandardItem(gIconManager::Instance()->GetIcon("int"),
                                                                 QString("%1").arg(itr.value().toDouble()));
                    itemProject->setEditable(false);
                    itemProject->setData(QVariant::Double,Qt::UserRole);

                    QList<QStandardItem*> list;
                    list<<itemProject<<itemvalue;
                    item->appendRow(list);
                    break;
                }
            default:
                qDebug()<<"itr type is "<<itr->type();
            }
        }
    return 0;
}

int JsonParser::renderSubJsonQArray(QJsonArray array, QStandardItem *model)
{
    if(nullptr == model)
        return -1;
    int i = 0;

    for(QJsonArray::iterator itr = array.begin();itr != array.end();itr++){
            QJsonArray arr;
            switch (itr->type()) {
                case QJsonValue::Type::Array:
                {
                    arr =  itr->toArray();
                    break;
                }
                case QJsonValue::Type::Object:
                {
                    QStandardItem* itemProject = new QStandardItem(gIconManager::Instance()->GetIcon("res/dir.svg"),QString::fromUtf8("项目"));
                    model->appendRow(itemProject);
                    renderSubJsonQModel(itr->toObject(),itemProject);
                    break;
                }
                case QJsonValue::Type::Bool:
                {
                    qDebug()<<itr->toBool();
                    if(itr->toBool()){
                        QStandardItem* itemProject = new QStandardItem(gIconManager::Instance()->GetIcon("missing"),QString::fromUtf8("true"));
                        itemProject->setData(QVariant(itr->toBool()),Qt::EditRole);
                        model->appendRow(itemProject);
                    }else{
                        QStandardItem* itemProject = new QStandardItem(gIconManager::Instance()->GetIcon("missing"),QString::fromUtf8("false"));
                        itemProject->setData(QVariant(itr->toBool()),Qt::EditRole);
                        model->appendRow(itemProject);
                    }
                    break;
                }
                case QJsonValue::Type::String:
                {
                    qDebug()<<itr->toString();
                    QStandardItem* itemProject = new QStandardItem(gIconManager::Instance()->GetIcon("tree_value_string"),itr->toString());
                    itemProject->setData(QVariant(itr->toString()),Qt::EditRole);
                    model->appendRow(itemProject);
                    break;
                }
                case QJsonValue::Type::Null:
                {
                    break;
                }
                case QJsonValue::Type::Double:
                {
                    QStandardItem* itemProject = new QStandardItem(gIconManager::Instance()->GetIcon("int"),QString("%1").arg(i));
                    QStandardItem* itemvalue = new QStandardItem(gIconManager::Instance()->GetIcon("int"),
                                                                 QString("%1").arg(itr->toDouble()));
                    itemProject->setEditable(false);
                    itemProject->setData(QVariant::Double,Qt::UserRole);

                    QList<QStandardItem*> list;
                    list<<itemProject<<itemvalue;
                    model->appendRow(list);

                }
            }
        }
    return 0;
}

QJsonObject getJsonObjectFromString(const QString jsonString){
    QJsonDocument jsonDocument = QJsonDocument::fromJson(jsonString.toUtf8().data());
    if( jsonDocument.isNull() ){
        qDebug()<< "===> please check the string "<< jsonString.toLocal8Bit().data();
    }
    QJsonObject jsonObject = jsonDocument.object();
    return jsonObject;
}
int JsonParser::_parseJson(QString dat)
{
   _getJsonObjectFromString(dat);

   return 0;
}

int JsonParser::_parseSubQVariant(QVariant v, QJsonObject *obj)
{
    if(nullptr == obj){
        return -1;
    }
    if(v.type() == QVariant::Map){
        QMap<QString,QVariant> map = v.toMap();
        QMap<QString,QVariant>::iterator itr = map.begin();
        for(;itr != map.end();itr++){
            if (itr.value() == QVariant::Map ){
                QJsonArray *sub = new QJsonArray();
                _parseSubQVariant(itr.value(),sub);
                obj->insert(itr.key(),QJsonValue(*sub));
            }else{
                obj->insert(itr.key(),_QVariantToQjson(*itr));
            }
        }
    }
    if(v.type() == QVariant::List){
        QList<QVariant> list = v.toList();
        QList<QVariant>::iterator itr = list.begin();
        int i = 0;
        for(;itr != list.end();itr++){
            if (itr->type() == QVariant::Map ){
                QJsonObject *sub = new QJsonObject();
                _parseSubQVariant(*itr,sub);
                //obj->insert(QString("%1").arg(i),QJsonValue(*sub));
            }
            if (itr->type() == QVariant::List){
                QJsonObject *sub = new QJsonObject();
                _parseSubQVariant(*itr,sub);
                //obj->insert(QString("%1").arg(i),QJsonValue(*sub));
                i ++;
            }
            else{
                obj->insert(QString("%1").arg(i),_QVariantToQjson(*itr));
            }
            i++;
        }
    }
}

int JsonParser::_parseSubQVariant(QVariant v, QJsonArray *arr)
{
    if(nullptr == arr){
        return -1;
    }
    if(v.type() == QVariant::Map){
        QMap<QString,QVariant> map = v.toMap();
        QMap<QString,QVariant>::iterator itr = map.begin();
        for(;itr != map.end();itr++){
            if (itr.value() == QVariant::Map){
                QJsonObject *sub = new QJsonObject();
                _parseSubQVariant(itr.value(),sub);
                arr->append(QJsonValue(*sub));
            }
            if (itr.value() == QVariant::Map){
                arr->append(QJsonValue::fromVariant(itr.value()));
            }
            if (itr.value() == QVariant::Map){
                arr->append(QJsonValue::fromVariant(itr.value()));
            }
        }
    }
    if(v.type() == QVariant::List){

    }
    if(v.type() == QVariant::Map){

    }
    return 0;
}

int JsonParser::_parseSubQVariant(QStandardItem *item, QJsonArray *arr)
{
    for(int row = 0;row < item->rowCount();row++){
        if(item->child(row,1)->data(Qt::UserRole).type() == QVariant::Map){
            QJsonObject *obj = new QJsonObject;
            this->_parseSubQVariant(item->child(row,1),obj);
            arr->append(QJsonValue(*obj));
        }
        else if(item->child(row,1)->data(Qt::UserRole).type() == QVariant::List){
            QJsonArray *arr = new QJsonArray;
            this->_parseSubQVariant(item->child(row,1),arr);
            arr->append(QJsonValue(*arr));
        }
        else{
            qDebug()<<item->child(row,1)->data(Qt::EditRole);
            arr->append(QJsonValue::fromVariant(item->child(row,1)->data(Qt::EditRole)));
        }
    }
    return 0;
}

int JsonParser::_parseSubQVariant(QStandardItem *item, QJsonObject *arr)
{
    for(int row = 0;row < item->rowCount();row++){
        if(item->child(row,1)->hasChildren()){

        }else{
            qDebug()<<item->child(row,1)->data(Qt::EditRole);
            arr->insert(item->child(row,0)->data(Qt::DisplayRole).toString(),
                        QJsonValue::fromVariant(item->child(row,1)->data(Qt::EditRole)));
        }
    }
}

QJsonValue JsonParser::_QVariantToQjson(QVariant v)
{
    switch (v.type()) {
        case QVariant::Bool:
            return QJsonValue(v.toBool());
        case QVariant::Int:
            return QJsonValue(v.toDouble());
        case QVariant::Map:
            return QJsonValue::fromVariant(v.toMap());
        case::QVariant::String:
            return QJsonValue::fromVariant(v.toString());
        case QVariant::List:
        return QJsonValue::fromVariant(v.toList());
    default:
        return QJsonValue(QJsonValue::Null);
    }
}


Parser::Parser()
{

}



void RangeYamlNode(YAML::Node node){
    if(node.IsScalar()){
        auto it= node.begin();
        qDebug()<< QString::fromStdString(node.Scalar());

    }else if (node.IsMap()){
        for(YAML::const_iterator it= node.begin(); it != node.end();++it)
        {
            qDebug()<< QString::fromStdString(it->first.as<string>())<< ":" << it->second.Type();
            RangeYamlNode(it->second);

        }
    }if (node.IsSequence()){
        for(YAML::const_iterator it= node.begin(); it != node.end();++it)
        {
            const YAML::Node& obj = *it;
            RangeYamlNode(obj);
        }
    }
}

YAML::Node getYamlNodeFromString(const QString yamlString){
    qDebug()<<yamlString;
    YAML::Node config = YAML::Load(yamlString.toLocal8Bit().data());
    RangeYamlNode(config);
    return config;
}

int YamlParser::Parse(QString conf)
{
    Parser::Parse(conf);
    qDebug()<<conf;
    if(mFile->fileName().contains("yaml")){
        if(!mFile->open(QFileDevice::ReadWrite)){
            return -1;
        }
        QString dat = QString(mFile->readAll());
        _parseYaml(dat);
    }else{

    }
    return 0;
}

int YamlParser::Parse(QStandardItemModel *myModel)
{
    auto tmpobj = YAML::Node();
    QModelIndex in = myModel->index(0, 0);
    if ( in.isValid() ) {
        for (int curIndex = 0; curIndex < myModel->rowCount(in); ++curIndex) {
            QModelIndex keyindex = myModel->index(curIndex, 0, in);
            QModelIndex dataindex = myModel->index(curIndex, 1, in);

            QStandardItem *keyItem = myModel->itemFromIndex(keyindex);

            QStandardItem *dataItem = myModel->itemFromIndex(dataindex);
            if(nullptr != keyItem) {
                if(!keyItem->hasChildren()){
                    mNode[keyItem->data(Qt::DisplayRole).toString().toStdString()] = _qvariantToNode(dataItem->data(Qt::EditRole));
                }else{
                    QVariant dat = keyItem->data(Qt::UserRole);
                    if(dat== QVariant::List){
                        YAML::Node *list = new YAML::Node;
                        this->_parseSubQVaraintArray(keyItem,list);
                        mNode[keyItem->data(Qt::DisplayRole).toString().toStdString().c_str()] = *list;
                    }
                    if(dat == QVariant::Map){
                        YAML::Node *map = new YAML::Node;
                        this->_parseSubQVaraintMap(keyItem,map);
                        mNode[keyItem->data(Qt::DisplayRole).toString().toStdString().c_str()] = *map;
                    }
                }
            }
        }
    }
}
int YamlParser::_parseSubQVaraintArray(QStandardItem *item, YAML::Node *arr)
{

    for (int curIndex = 0; curIndex < item->rowCount(); ++curIndex) {

        QStandardItem *keyItem = item->child(curIndex,0);
        QStandardItem *dataItem = item->child(curIndex,1);
        if(nullptr != keyItem) {
            if(!keyItem->hasChildren()){
                ((YAML::Node)(*arr)).push_back(_qvariantToNode(dataItem->data(Qt::EditRole)));

            }else{
                QVariant dat = keyItem->data(Qt::UserRole);
                if(dat.type() == QVariant::List){
                    YAML::Node *list = new YAML::Node;
                    this->_parseSubQVaraintArray(keyItem,list);
                    (YAML::Node)(*arr)[keyItem->data(Qt::DisplayRole).toString().toStdString().c_str()] = *list;
                }
                if(dat.type() == QVariant::Map){
                    YAML::Node *map = new YAML::Node;
                    this->_parseSubQVaraintMap(keyItem,map);
                    (YAML::Node)(*arr)[keyItem->data(Qt::DisplayRole).toString().toStdString().c_str()] = *map;
                }
            }
        }
    }
    return 0;

}

int YamlParser::_parseSubQVaraintMap(QStandardItem *item, YAML::Node *node)
{
    for (int curIndex = 0; curIndex < item->rowCount(); ++curIndex) {
        QStandardItem *keyItem = item->child(curIndex,0);
        QStandardItem *dataItem = item->child(curIndex,1);
        if(nullptr != keyItem) {
            if(!keyItem->hasChildren()){
                (YAML::Node)(*node)[keyItem->data(Qt::DisplayRole).toString().toStdString()] = _qvariantToNode(dataItem->data(Qt::EditRole));
            }else{
                QVariant dat = keyItem->data(Qt::UserRole);
                if(dat.type() == QVariant::List){
                    YAML::Node *list = new YAML::Node;
                    this->_parseSubQVaraintArray(keyItem,list);
                    (YAML::Node)(*node)[keyItem->data(Qt::DisplayRole).toString().toStdString().c_str()] = *list;
                }
                if(dat.type() == QVariant::Map){
                    YAML::Node *map = new YAML::Node;
                    this->_parseSubQVaraintMap(keyItem,map);
                    (YAML::Node)(*node)[keyItem->data(Qt::DisplayRole).toString().toStdString().c_str()] = *map;
                }
            }
        }
    }
    return 0;
}



void YamlParser::RenderQModelObject(QStandardItem *item)
{
    if (nullptr == item){
        return;
    }
    for(YAML::const_iterator it= mNode.begin(); it != mNode.end();it++)
    {
        if(it->second.IsScalar()){
            QStandardItem* itemkey = new QStandardItem(gIconManager::Instance()->GetIcon("tree_value_string"),
            QString::fromStdString(it->first.as<string>()));
            QStandardItem* itemvalue = new QStandardItem(gIconManager::Instance()->GetIcon("tree_value_string"),
                                                                     QString::fromStdString(it->second.Scalar()));
            itemvalue->setData(_guessType(QString::fromStdString(it->second.Scalar())),
                               Qt::EditRole);
            itemkey->setEnabled(false);
            QList<QStandardItem*> list;
            list<<itemkey<<itemvalue;
            item->appendRow(list);
        }else if(it->second.IsSequence() || it->second.IsMap()){
            QStandardItem* itemProject = new QStandardItem(gIconManager::Instance()->GetIcon("res/dir.svg"),
                QString::fromStdString(it->first.as<string>()));

            qDebug()<<"key" <<QString::fromStdString(it->first.as<string>());
            if(it->second.IsSequence()){
                QList<QVariant> listv;
                renderSubYamlQModel(listv,it->second,itemProject);
                QVariant v(listv);
                qDebug()<<"sequence value"<<v<<v.type();
                itemProject->setData(v.type(),Qt::UserRole);
                itemProject->setData(QString::fromStdString(it->first.as<string>()),Qt::DisplayRole);
            }
            if(it->second.IsMap()){
                QMap<QString,QVariant> vmap;
                renderSubYamlQModel(vmap,it->second,itemProject);
                QVariant v(vmap);
                qDebug()<<"map value"<<v<<v.type();
                itemProject->setData(v.type(),Qt::UserRole);
                itemProject->setData(QString::fromStdString(it->first.as<string>()),Qt::DisplayRole);
            }
            item->appendRow(itemProject);
            itemProject->setEditable(false);

        }
    }
}


YAML::Node& YamlParser::Node()
{
    return mNode;
}

int YamlParser::renderSubYamlQModel(QMap<QString, QVariant> &map, YAML::Node node, QStandardItem *item)
{
        auto itr = node.begin();
        for(YAML::const_iterator it= node.begin(); it != node.end();it++)
        {
            if(it->second.IsScalar()){
                    QStandardItem* itemkey = new QStandardItem(gIconManager::Instance()->GetIcon("tree_value_string"),
                                                               QString::fromStdString(it->first.as<string>()));
                    itemkey->setEditable(false);
                    QStandardItem* itemvalue = new QStandardItem(gIconManager::Instance()->GetIcon("tree_value_string"),
                                                                 QString::fromStdString(it->second.Scalar()));
                    itemvalue->setData(_guessType(QString::fromStdString(it->second.Scalar())),
                                       Qt::EditRole);
                    map[QString::fromStdString(itr->first.as<string>())] = _guessType(QString::fromStdString(it->second.Scalar()));
                    QList<QStandardItem*> list;
                    list<<itemkey<<itemvalue;
                    item->appendRow(list);
             }else if(it->second.IsSequence() || it->second.IsMap()) {
                QStandardItem* itemProject = new QStandardItem(gIconManager::Instance()->GetIcon("res/dir.svg"),
                                                               QString::fromStdString(it->first.as<string>()));
                itemProject->setEditable(false);
                if(it->second.IsSequence()){
                    QList<QVariant> listv;
                    renderSubYamlQModel(listv,it->second,itemProject);
                    QVariant v(listv);
                    itemProject->setData(v,Qt::EditRole);
                    itemProject->setData(QString::fromStdString(it->first.as<string>()),Qt::DisplayRole);
                }
                if(it->second.IsMap()){
                    QMap<QString,QVariant> vmap;
                    renderSubYamlQModel(vmap,it->second,itemProject);
                    QVariant v(vmap);
                    itemProject->setData(v.type(),Qt::EditRole);
                    itemProject->setData(QString::fromStdString(it->first.as<string>()),Qt::DisplayRole);
                }
                item->appendRow(itemProject);
            }
        }
    return 0;
}

int YamlParser::renderSubYamlQModel(QList<QVariant> &vlist, YAML::Node node, QStandardItem *item)
{
    auto itr = node.begin();
    int index = 0;
    for(YAML::const_iterator it= node.begin(); it != node.end();it++)
    {
        if(it->IsScalar()) {
            QStandardItem* itemkey = new QStandardItem(gIconManager::Instance()->GetIcon("tree_value_string"),
                                                               QString("%1").arg(index));
            itemkey->setEditable(false);
            QStandardItem* itemvalue = new QStandardItem(gIconManager::Instance()->GetIcon("tree_value_string"),
                                                                 QString::fromStdString(it->Scalar()));
            itemvalue->setData(this->_guessType(QString::fromStdString(it->Scalar())),Qt::EditRole);
            QList<QStandardItem*> list;
            vlist<<this->_guessType(QString::fromStdString(it->Scalar()));
            list<<itemkey<<itemvalue;
            item->appendRow(list);
     }else if(it->IsSequence() || it->IsMap()) {
            QStandardItem* itemProject1 = new QStandardItem(gIconManager::Instance()->GetIcon("res/dir.svg"),
                    QString("%1").arg(index));
            if(it->second.IsSequence()){
                QList<QVariant> listv;
                renderSubYamlQModel(listv,it->second,itemProject1);
                QVariant v(listv);
                qDebug()<<"child sequence"<<v;
                itemProject1->setData(v,Qt::EditRole);
                itemProject1->setData(QString::fromStdString(it->first.as<string>()),Qt::DisplayRole);
            }
            if(it->second.IsMap()){
                QMap<QString,QVariant> vmap;
                renderSubYamlQModel(vmap,it->second,itemProject1);
                QVariant *v = new QVariant(vmap) ;
                qDebug()<<"child map"<<v;
                itemProject1->setData(*v,Qt::EditRole);
                itemProject1->setData(QString::fromStdString(it->first.as<string>()),Qt::DisplayRole);
            }
            item->appendRow(itemProject1);
        }
        index++;
    }
    return 0;
}

YAML::Node YamlParser::_qvariantToNode(QVariant value)
{
    YAML::Node ret;
    switch (value.type()) {
    case QVariant::Int:
        ret = value.toInt();
        return ret;
    case QVariant::Bool:
        ret = value.toBool();
        return ret;
    case QVariant::String:
        YAML::Node ret = YAML::Load(value.toString().toStdString());
        return ret;
    }
    return YAML::Node();

}

YAML::Node YamlParser::getYamlNodeFromString(const QString yamlString)
{
    mNode = YAML::Load(yamlString.toLocal8Bit().data());
    return mNode;
}

int YamlParser::_parseYaml(QString dat) noexcept
{
    getYamlNodeFromString(dat);
    return 0;
}

QVariant YamlParser::_guessType(QString dat)
{
    if(dat.contains(QRegExp("^\\d+$"))){
        return QVariant(dat.toInt());
    }
    if(dat.contains(QRegExp("^-?\\d{1,}(\\.[0-9]+)?"))){
        return QVariant(dat.toFloat());
    }
    if(dat == "true"){
        return QVariant(true);
    }
    if(dat == "false"){
        return QVariant(false);
    }
    return QVariant(dat);
}

XmlParser::XmlParser():
    Parser(),
    mDoc(nullptr),
    mCur(nullptr)
{
}

int XmlParser::Parse(QString conf)
{
    Parser::Parse(conf);
    qDebug()<<conf;
    if(mFile->fileName().contains("xml")){
        _parseXML(mFile->fileName());
    }else{

    }
    return 0;
}

void XmlParser::RenderQModelObject(QStandardItem *model)
{
    if(nullptr == model){
        return;
    }
    xmlNode *cur_node = xmlDocGetRootElement(mDoc);
    qDebug()<<"new round"<<"and the type is "<<model->type();
    for (;cur_node;cur_node = cur_node->next)
    {
        if (cur_node->type == XML_ELEMENT_NODE)
        {
            QStandardItem* itemkey = new QStandardItem(gIconManager::Instance()->GetIcon("tree_value_string"),
                                                       QString::fromStdString(std::string((char*)cur_node->name)));
            qDebug("node type: Element, name: %s", cur_node->name);
            itemkey->setEditable(false);
            model->appendRow(itemkey);
        }
        else if(cur_node->type == XML_TEXT_NODE)
        {
            if(cur_node->content[0] == 10) {
            }else{
                qDebug("text node: %s %s %ld",cur_node->name,xmlNodeGetContent(cur_node),cur_node->children);//获取节点值
            }
        }
        else if(cur_node->type == XML_DTD_NODE){
            qDebug("text node: %s",xmlNodeGetContent(cur_node));//获取节点值
        }
        struct _xmlAttr * tmp = cur_node->properties;
        while(tmp != nullptr){
            qDebug("attribute type:%d  : %s: %s ",tmp->type,tmp->name,tmp->children->content);//获取节点值
            tmp = tmp->next;
        }
        QStandardItem* itemkeysub = new QStandardItem(gIconManager::Instance()->GetIcon("tree_value_string"),
                                                   QString::fromStdString(std::string((char*)cur_node->name)));
        renderSubXmlQModel(cur_node->children,itemkeysub);
        model->appendRow(itemkeysub);
    }
}


XmlParser::~XmlParser()
{
    xmlFreeDoc(mDoc);
}

int XmlParser::_parseXML(QString document) noexcept
{
    mDoc = xmlReadFile(document.toStdString().c_str(),"GB2312",XML_PARSE_RECOVER); //解析文件
}

int XmlParser::renderSubXmlQModel(xmlNode *cur_node, QStandardItem *model)
{
    if(nullptr == cur_node || nullptr == model)
        return -1;
    qDebug()<<"new round"<<"and the type is "<<cur_node->type;
    for (;cur_node != nullptr;cur_node = cur_node->next)
    {
        if (cur_node->type == XML_ELEMENT_NODE)
        {
            QStandardItem* itemkey = new QStandardItem(gIconManager::Instance()->GetIcon("tree_value_string"),
                                                       QString::fromStdString(std::string((char*)cur_node->name)));
            qDebug("node type: Element, name: %s", cur_node->name);
            itemkey->setEditable(false);
            model->appendRow(itemkey);
        }
        else if(cur_node->type == XML_TEXT_NODE)
        {
            qDebug()<<"text length "<<strlen((char*)xmlNodeGetContent(cur_node));
            qDebug("text node: %s %ld",xmlNodeGetContent(cur_node),cur_node->children);//获取节点值

        }
        if(cur_node->properties != nullptr){
            qDebug("attribute text: %s",cur_node->properties->name);//获取节点值
        }
        else if(cur_node->type == XML_DTD_NODE){
            qDebug("text node: %s",xmlNodeGetContent(cur_node));//获取节点值
        }
        struct _xmlAttr * tmp = cur_node->properties;
        while(tmp != nullptr){
            qDebug("attribute type:%d  : %s: %s ",tmp->type,tmp->name,tmp->children->content);//获取节点值
            tmp = tmp->next;
        }
        QStandardItem* itemkeysub = new QStandardItem(gIconManager::Instance()->GetIcon("tree_value_string"),
                                                   QString::fromStdString(std::string((char*)cur_node->name)));
        renderSubXmlQModel(cur_node->children,itemkeysub);
    }
}
