#ifndef GLOBAL_H
#define GLOBAL_H
#include <map>
#include <QString>
#include <QIcon>
#include "pattern/signleton.hpp"

using namespace std;


typedef  class CIConMnagager{
public:
    CIConMnagager();
    QIcon GetIcon(QString);
private:
    map<QString,QIcon> gPublicIconMap;
}IConMnagager;

typedef Singletone<CIConMnagager> gIconManager;

#endif // GLOBAL_H

