#include "mainwindow.h"

#include <QApplication>
#include <QJsonObject>
#include <QJsonDocument>
#include <QDebug>
#include <QStringList>
#include <QFile>
#include <QJsonDocument>
#include <QMetaObject>
#include <fileparser.h>
#include <QScreen>
#include "mask_widget.h"

void initQss()
{
    QFile file(":/qss/css/qss.css");
    if (!file.open(QIODevice::ReadOnly))
        exit(0);
    QTextStream in(&file);
    QString css = in.readAll();
    qApp->setStyleSheet(css);
    qApp->setFont(QFont("微软雅黑", 15));
    return;
}

int RegiesterOwnType(){
    return 0;
}

int main(int argc, char *argv[])
{
#if (QT_VERSION >= QT_VERSION_CHECK(5,9,0))
    QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif
    QApplication a(argc, argv);
    MainWindow w(nullptr,1);

    w.setWindowTitle("通用配置管理工具");
    w.SetTitleHeight(45);
    w.show();
    mask_widget mask(&w);
    mask.show();

    return a.exec();
}
